# FAF4 Data Analysis
This repository stores scripts that disaggregate national FAF4 datasets to the county level and analyze and visualize FAF4 commodity flow data at the county level.  
 
## FAF4
FAF is a free public data model produced by the U.S. Department of Transportation to assist freight planning. FAF uses Commodity Flow Survey and other federal information to provide estimates on freight tonnage and freight value for 42 broad commodity groups and different transportation modes. The latest FAF database (FAF4) has 2012 as the
base year with forecasts to 2045 and commodity truck-trips assigned to highway networks and major routes.  

Although FAF4 data is the most comprehensive and accurate data regarding freight movement that is available in the field, it has a few limitations. FAF4 data is limited to 50 states and 132 “business economic area” zones. Champaign County data is combined with data for other non-Chicago-area counties and non-St. Louis-area counties in Illinois in the original FAF4 database.   

## Truck Commodity Flow Analysis
For Champaign County truck commodity flows, a modified version of the FAF4 database developed for IDOT by transportation consultant WSP is used. The data breaks down the FAF4 truck commodity flows to the county level, for analysis years 2017 (extrapolated from 2012) and 2045.  

* TruckCommodityFlowAnalysis_Tonnage.R
* TruckCommodityFlowAnalysis_Value.R

**Top Commodities by Tonnage**  
![Top Commodities by Tonnage](TopCommoditiesByTonnage.png)

**Top Trading Partners**  
![Top Trading Partners](TopTradingPartners.png)

**Commodity Value Increase**  
![Commodity Value Increase](CommodityValueIncrease.png)

## Rail Commodity Flow Disaggregation and Analysis
For Champaign County rail commodity flows, a disaggregation method that uses variables including population, employment by industry, and land use to break down the FAF4 national rail commodity flow database to the county level for Illinois and the state level for the rest of the country for each commodity type is developed. IDOT’s county level tonnage by directions information, which is based on Surface Transportation Board’s Confidential Waybill Sample2, is used to validate the disaggregation results.  

* FAF4RailDisaggregation.R
* RailCommodityFlowAnalysis.R

## FAF4 Data Limitations
The disaggregated data can be used to calculate inbound, outbound, and internal freight tonnage and value, but not for pass-through freight activities. In addition, FAF4 data cannot capture all freight movements, such as those conducted by in-house fleet. Many commodities such as timber, some farm products, fisheries products, or solid waste cannot be completely calculated either.

## Author
FAF4 data analysis scripts were developed by Shuake Wuzhati at the Champaign County Regional Planning Commission (CCRPC) as part of the Champaign-Urbana Region Freight Plan.

## License
This project is licensed under BSD 3-Clause License - see the LICENSE.md file for details.
